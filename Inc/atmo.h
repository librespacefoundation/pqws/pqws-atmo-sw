/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATMO_H_
#define ATMO_H_

//Subsystem ID 0 - 31
#define PQ_ATMO_NODE_BASEID 0x04 << 6

typedef enum {
  PQ_ATMO_PING = 0,
  PQ_ATMO_PREHEAT_MQ7,
  PQ_ATMO_PREHEAT_MQ131,
  PQ_ATMO_PREHEAT_MICS,
  PQ_ATMO_HEATER,
  PQ_ATMO_FAN,
  PQ_ATMO_INSTANT_READ,
  PQ_ATMO_SINGLE_READ,
  PQ_ATMO_CONTINUOUS_READ
} atmo_commands_t;

typedef enum {
  PQ_ATMO_TEMPERATURE=0,
  PQ_ATMO_HUMIDITY,
  PQ_ATMO_PRESSURE,
  PQ_ATMO_MQ7,
  PQ_ATMO_MQ131,
  PQ_ATMO_MICS
} atmo_sensor_t;

#endif /* ATMO_H_ */
