/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SENSORS_H_
#define SENSORS_H_
#include "stm32l4xx_hal.h"
#include "atmo.h"

//#define ADC_CHANNEL_V2 ADC_CHANNEL_6
#define ADC_CHANNEL_MQ_7 ADC_CHANNEL_15
#define ADC_CHANNEL_MQ_131 ADC_CHANNEL_10
#define ADC_CHANNEL_MICS ADC_CHANNEL_8
//#define ADC_CHANNELVBAT ADC_CHANNEL_18
#define ADC_CHANNEL_TEMP ADC_CHANNEL_17

#define MQ_7_HI_TIME	60000
#define MQ_7_LOW_TIME	90000
#define MQ_7_HI_PWM		2000
#define MQ_7_LOW_PWM	100

#define SENSOR_CO_CALC 0.805664063
#define SENSOR_O3_CALC 0.805664063
#define SENSOR_MICS_CALC 0.805664063

typedef struct {
	uint16_t min;
	uint16_t max;
	uint16_t average;
	atmo_sensor_t sensor_type;
} pq_atmo_measurement_t;

typedef enum {
	POWER_OFF=0,
	POWER_LOW,
	POWER_HIGH,
	POWER_ON
} sensor_power_t;

uint16_t sensor_read_CO(ADC_HandleTypeDef *adc);
uint16_t sensor_read_O3(ADC_HandleTypeDef *adc);
uint16_t sensor_read_MICS(ADC_HandleTypeDef *adc);
void sensor_power_off();
void sensor_MQ7_power(sensor_power_t state);
void sensor_MQ131_power(sensor_power_t state);
void sensor_MICS_power(sensor_power_t state);
void fan_power(uint8_t power);
void fan_disable();
void fan_test();
void heater_test_O3();
void heater_test_CO();
void preheat_MQ7(uint8_t hours);
void preheat_MQ131(uint8_t hours);
void sensor_CO_heater_sequence();
void sensor_power(atmo_sensor_t sensor, sensor_power_t power);
#endif /* SENSORS_H_ */
